const express = require('express')
const axios = require('axios')

const router = express.Router()

axios.defaults.headers['Authorization'] = `Bearer ${process.env.PUBG_API_KEY}`
axios.defaults.headers['Accept'] = 'application/vnd.api+json'
class API {
    getPlayerInfo(request, response) {
        axios.get(`https://api.pubg.com/shards/pc-na/players?filter[playerNames]=${request.params.username}`).then(
            axiosResponse => {
                console.log(axiosResponse.data)
                response.send(axiosResponse.data)
            }
        ).catch(error => {
            console.log(error.message)
            response.send(error.message)
        })
    }
}

const api = new API()
/* GET home page. */
router.get('/players/:username', api.getPlayerInfo);

module.exports = router;
